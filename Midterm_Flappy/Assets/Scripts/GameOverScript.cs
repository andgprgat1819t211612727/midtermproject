﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameOverScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene("GamePlay");
        PlayerScript.isOkayToPlay = true;
        TextScript.playerScore = 0;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
