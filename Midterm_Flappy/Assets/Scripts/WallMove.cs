﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallMove : MonoBehaviour
{
    public float speed;
    private float lifeTime = 13;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerScript.isOkayToPlay)
        {
            this.transform.position = new Vector3(this.transform.position.x + (-speed * Time.deltaTime), this.transform.position.y, this.transform.position.z);
            if (lifeTime <= 0)
            {
                Destroy(gameObject);
                Debug.Log("Destroy");
            }
        }
        Debug.Log(lifeTime);
        lifeTime -= Time.deltaTime;
    }
}
