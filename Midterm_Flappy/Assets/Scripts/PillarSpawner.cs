﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PillarSpawner : MonoBehaviour
{
    public float spawnTime;
    public float spawnDelay;
    public int playerScore;

    public GameObject pillar;
    public Transform spawnPoint;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnPillar", spawnTime, spawnDelay);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnPillar()
    {
        if (PlayerScript.isOkayToPlay)
        {
            Instantiate(pillar, new Vector3(spawnPoint.transform.position.x, Random.Range(-3.6f, 3.6f), spawnPoint.transform.position.z), transform.rotation);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Point")
        {
            playerScore++;
        }
    }
}
