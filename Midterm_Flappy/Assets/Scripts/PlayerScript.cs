﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    private Rigidbody2D rb;
    private IEnumerator wait;

    public float jumpForce;
    public static bool isOkayToPlay = true;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if (isOkayToPlay)
            {
                rb.velocity = Vector2.up * jumpForce;
            }
        }
        wait = WaitAndQuit(2.0f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Point")
        {
            TextScript.playerScore += 1;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Wall")
        {
            isOkayToPlay = false;
            StartCoroutine(wait);
        }
        if (collision.gameObject.tag == "Wall2")
        {
            isOkayToPlay = false;
            StartCoroutine(wait);
        }
    }

    IEnumerator WaitAndQuit(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        SceneManager.LoadScene("GameOver");
    }
}
